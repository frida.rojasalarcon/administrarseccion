


<%@page import="java.util.ArrayList"%>
<%@page import= "com.develop.model.PropietarioM"%>
<%@page import= "com.develop.DAO.PropietarioDAO"%>
<%@page import="com.develop.model.Propiedad"%>

<%@include file="../HeaderGeneral.jsp"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="../EstiloVista.css">


<nav style="background-color: transparent;">
	<form class="form-inline my-2 my-lg-0">
		<input class="form-control mr-sm-2" type="search" placeholder="Search"
			aria-label="Search">
		<button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>

							<%
							PropietarioDAO dao=new PropietarioDAO();
							
							String curp=request.getParameter("curp");
							%>

	</form>

	<span class="navbar-text">
		<form method="POST" action=<%="http://localhost:8080/administracionSeccion/vistas/Propiedades/AgregaPropiedades.jsp?curp="+curp%>>
			<button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Agregar
				Propiedad</button>
		</form>
	</span>

</nav>

</head>
<body>



	<link
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
		rel="stylesheet">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-box clearfix">
					<div class="table-responsive">
						<table class="table user-list">
							<thead>
								<tr>
									<th><span>Direccion</span></th>
									<th><span>Estatus</span></th>
									<th><span>Caseta</span></th>
									<th><span>Identificador</span></th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
							<%
							for(PropietarioM a: dao.ListaPropiedades()){
								if(a.getCurp().equals(curp)){
									Propiedad[] propiedad=a.getPropiedad();
									for(Propiedad p:propiedad ){
								
									
							%>
								<tr>
									<td><%= p.getDireccion() %></td>
									<td><%= p.getEstatus() %></td>
									<td><%= p.getCaseta() %></td>
									<td><%= p.getIdentificador() %></td>
									
									<td style="width: 20%;">
										

										<form method="POST" action=<%="http://localhost:8080/administracionSeccion/PropietarioC?curp="+curp+"&identificador="+p.getIdentificador()%>>
											<button type="submit" name="borrarPP" id="borrarPP" 
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>


										<form method="POST" action="">
											<button type="submit" name="modificar" onclick="http://localhost:8080/CentroComercial/UsuarioC"
												class="btn btn-outline-info btn-circle btn-lg btn-circle ml-2">
												<i class="fa fa-trash"></i>
											</button>
										</form>

									</td>
								</tr>
								
								<%} }}%>
							</tbody>
						</table>
					</div>
					<ul class="pagination pull-right">
						<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
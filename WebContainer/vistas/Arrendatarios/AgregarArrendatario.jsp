<%@page import="java.util.ArrayList"%>
<%@include file="../HeaderGeneral.jsp"%>
<%@page import="com.develop.DAO.ArrendatarioDAO"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
	crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/administracionSeccion/vistas/estiloAgrega.css">
</head>
<body>

	<div class="container">
		<div class="row gutters">
			<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
				<div class="card h-100">
					<div class="card-body">
						<div class="account-settings">
							<div class="user-profile">
								<div class="user-avatar">
									<img
										src="https://media.istockphoto.com/photos/portrait-of-smiling-handsome-man-in-blue-tshirt-standing-with-crossed-picture-id1045886560?s=612x612"
										alt="Maxwell Admin">
								</div>
								<h5 class="user-name">¡Hola!</h5>
								<h6 class="user-email">Estas Registrando un nuevo Arredantario</h6>
							</div>
							<div class="about">
								<h5 class="mb-2 text-primary">About</h5>
								<p>Gracias por tu excelente trabajo!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
				<div class="card h-100">
				<form action="<%=request.getContextPath()%>/ArrendatarioC">
					<div class="card-body">
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<h6 class="mb-3 text-primary">Datos del arrendatario</h6>
							</div>
							

								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Nombre o nombres</label> <input type="text"
											class="form-control" id="nombre" name="nombre"
											placeholder="Escribe el nombre" >
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Apellidos</label> <input type="text"
											class="form-control" id="apellido" name="apellido"
											placeholder="Escribe los apellidos">
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>CURP</label> <input type="text"
											class="form-control" id="curp" name="curp"
											placeholder="Escribe la CURP">
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Edad</label> <input type="text"
											class="form-control" id="edad" name="edad"
											placeholder="Escribe la edad del arrendatario">
									</div>
								</div>
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<h6 class="mb-3 text-primary">Elige la casa para rentar</h6>
								</div>
								
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
									    <label>Seleccione Propiedad<small>   propiedad-->propietario</small></label>
									     <select name="propiedad" id="propiedad">
									     <optgroup >
									      <p>
									       <%
									       ArrendatarioDAO dao=new ArrendatarioDAO();
									              for (String[] pp : dao.propiedadesDisponibles()) {
									                     %><option><%=pp[0]+"<--->"+pp[1]%></option>
									       <%}%>
									      </p>
									     </optgroup>
									    </select>
									   </div>
								</div>
								
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<h6 class="mb-3 text-primary">Datos del Vehiculo</h6>
								<div class="form-check">
							        <input class="form-check-input" type="checkbox" id="check" name="check">
							        <label class="form-check-label" for="gridCheck1">
							          Si tengo vehiculo!
							        </label>
							      </div>
								</div>
								
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Matricula</label> <input type="text"
											class="form-control" id="matricula" name="matricula"
											placeholder="Escribe la matricula del vehiculo">
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Modelo</label> <input type="text"
											class="form-control" id="modelo" name="modelo"
											placeholder="Escribe el modelo del vehiculo">
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
										<label>Marca</label> <input type="text"
											class="form-control" id="marca" name="marca"
											placeholder="Escribe el modelo del vehiculo">
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="form-group">
									    <label>Seleccione caseta</label>
									     <select name="caseta" id="caseta">
									     <optgroup >
									      <p>
									       <option>1</option>
									       <option>2</option>
									       <option>3</option>
									       <option>4</option>
									      </p>
									     </optgroup>
									    </select>
									   </div>
								</div>
						</div>
						<div class="row gutters">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="text-right">
									<button  id="agregarA" name="agregarA"  type="submit"
										class="btn btn-primary">Agregar</button>
								</div>
							</div>
						</div>

					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
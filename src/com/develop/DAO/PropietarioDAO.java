package com.develop.DAO;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.develop.config.ConexionDB;
import com.develop.model.Propiedad;
import com.develop.model.PropietarioM;
import com.develop.model.Vehiculo;
import com.google.gson.Gson;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

public class PropietarioDAO {

	public String insertaPropietario(PropietarioM propietariom, Propiedad propiedadm, Vehiculo vehiculom, String tiene){
		 
		 ConexionDB con=new ConexionDB();
		 
	        try {           
	        	
	        	MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
	            //(A�adimos un documento a la base de datos directamente).
	        	collection.insertOne(
	                    new Document("nombre", propietariom.getNombre())
	                                 .append("apellidos", propietariom.getApellidos())
	                                  .append("curp", propietariom.getCurp())
	                                  .append("edad", propietariom.getEdad())
	                                  .append("propiedad",asList(
	                                          new Document()
	                                            .append("direccion", propiedadm.getDireccion())
	                                            .append("estatus", propiedadm.getEstatus())
	                                            .append("caseta", propiedadm.getCaseta() )
	                                            .append("identificador", propiedadm.getIdentificador() )
	                                		  ))
	                                  
	                                .append("vehiculo", asList()
	        			));
	        	if(tiene!=null) {
	        		agregarVehiculo(propietariom.getCurp(),vehiculom.getMatricula(), vehiculom.getMarca(),vehiculom.getModelo(), vehiculom.getCaseta());
	        	}
	        	
	        	System.out.println("Registro Exitosooooooo");
	            return "Exito";
	        	
	        } catch (Exception ex) {
	            //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	        	return "Fallo la conexion"+ex;
	        }        
	    }

	public List<PropietarioM> ListaPropiedades(){
		 ConexionDB con=new ConexionDB();
		 ArrayList<PropietarioM> lista=new ArrayList();
		 Gson gson = new Gson();
		 
	        try {           
	        	
	        	MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
	        	
	        	FindIterable<Document> doc = collection.find();
	        	
	        	for (Document d : doc) {
					PropietarioM prop=new PropietarioM();
					
					
					prop=gson.fromJson(d.toJson(), PropietarioM.class);
					lista.add(prop);
	        		
				}
	        	
	        	
	        }catch(Exception e) {
	        	System.out.println(e);
	        }
		 
		 return lista;
	 }
	 
	public String agregarVehiculo(String curp, String matricula, String marca, String modelo, String caseta) {
	 	
		ConexionDB con=new ConexionDB();
		
		Bson filter =Filters.and(Filters.eq("curp",curp));
		
		System.out.println(curp+" "+matricula+" "+marca);
		
		Document newVehiculo = new Document()
				.append("matricula", matricula)
				.append("marca", marca)
				.append("modelo", modelo)
				.append("caseta", caseta);

		
		
	 try {
		 MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
		 collection.updateOne(filter,Updates.addToSet("vehiculo", newVehiculo));
		 return "Exito";
	} catch (Exception e) {
		return "Error"+e;
		// TODO: handle exception
	}
	 
}
	
	public String agregarPropiedad(String curp, String direccion, String estatus, String caseta, String identificador) {
	 	
		ConexionDB con=new ConexionDB();
		
		Bson filter =Filters.and(Filters.eq("curp",curp));
		
		System.out.println("curp:"+curp+" direccion:"+direccion+" identificador"+identificador);
		
		Document newVehiculo = new Document()
				.append("direccion", direccion)
				.append("estatus", estatus)
				.append("caseta", caseta)
				.append("identificador", identificador);

		
		
	 try {
		 MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
		 collection.updateOne(filter,Updates.addToSet("propiedad", newVehiculo));
		 return "Exito";
	} catch (Exception e) {
		return "Error"+e;
		// TODO: handle exception
	}
	 
}

	public void eliminaPropietario(String curp) {
		ConexionDB con=new ConexionDB();


		try {           
			System.out.println("el curp en dao es:"+curp);
	       	MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
	       	Bson filter =Filters.and(Filters.eq("curp",curp));
	       	
	       	collection.findOneAndDelete(filter);
	       	
		} catch (Exception ex) {
	            //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	        	System.out.print("Fallo la conexion");
	        }
		    
		  }
		
	public void eliminaV(String curp,String matricula) {
		ConexionDB con=new ConexionDB();


		try {           
			System.out.println("el curp en dao es:"+curp);
       	MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
       	Bson filter =Filters.and(Filters.eq("curp",curp));
       	collection.updateOne(filter,Updates.pull("vehiculo", new Document("matricula",matricula)));

		} catch (Exception ex) {
           //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
       	System.out.print("Fallo la conexion");
       }
	    
	  }
	
	public void eliminaPropiedad(String curp,String identificador) {
		ConexionDB con=new ConexionDB();


		try {           
			System.out.println("el curp en daoPP es:"+curp+"  el identificador: "+identificador);
       	MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
       	Bson filter =Filters.and(Filters.eq("curp",curp));
       	collection.updateOne(filter,Updates.pull("propiedad", new Document("identificador",identificador)));

		} catch (Exception ex) {
           //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
       	System.out.print("Fallo la conexion");
       }
	    
	  }
	
	public void updateP(String nombre, String curp) {
		ConexionDB con=new ConexionDB();
		
		Bson filter =Filters.and(Filters.eq("curp",curp));
		Bson update = Updates.set("nombre", nombre);
		
		try {
			 MongoCollection<Document> collection = con.crearConexion().getCollection("Propietarios");
			 collection.updateOne(filter, update, null);
			 
		}catch(Exception e) {
			
		}
		
		
	  }
}

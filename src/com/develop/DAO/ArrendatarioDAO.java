package com.develop.DAO;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.develop.config.ConexionDB;
import com.develop.model.Arrendatario;
import com.develop.model.Propiedad;
import com.develop.model.PropietarioM;
import com.develop.model.Vehiculo;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.PushOptions;
import com.mongodb.client.model.Updates;

public class ArrendatarioDAO {

	public String insertaArrendatario(Arrendatario arrendatariom, Vehiculo vehiculom, String tiene){
		 
		 ConexionDB con=new ConexionDB();
		 
	        try {           
	        	
	        	MongoCollection<Document> collection = con.crearConexion().getCollection("Arrendatarios");
	            //(A�adimos un documento a la base de datos directamente).
	        	collection.insertOne(
	                    new Document("nombre", arrendatariom.getNombre())
	                                 .append("apellidos", arrendatariom.getApellidos())
	                                  .append("curp", arrendatariom.getCurp())
	                                  .append("edad", arrendatariom.getEdad())
	                                .append("propiedadRentada", arrendatariom.getPropiedadRentada() )
	                                .append("vehiculo", asList()
	        			));
	        	if(tiene!=null) {
	        		agregarVehiculo(arrendatariom.getCurp(),vehiculom.getMatricula(), vehiculom.getMarca(),vehiculom.getModelo(), vehiculom.getCaseta());
	        	}
	        	
	        	System.out.println("Registro Exitosooooooo");
	            return "Exito";
	        	
	        } catch (Exception ex) {
	            //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	        	return "Fallo la conexion"+ex;
	        }        
	    }

	public List<Arrendatario> ListaArrendatarios(){
		 ConexionDB con=new ConexionDB();
		 ArrayList<Arrendatario> lista=new ArrayList();
		 Gson gson = new Gson();
		 
	        try {           
	        	
	        	MongoCollection<Document> collection = con.crearConexion().getCollection("Arrendatarios");
	        	
	        	FindIterable<Document> doc = collection.find();
	        	
	        	for (Document d : doc) {
					Arrendatario prop=new Arrendatario();
					
					
					prop=gson.fromJson(d.toJson(), Arrendatario.class);
					lista.add(prop);
	        		
				}
	        	
	        	
	        }catch(Exception e) {
	        	System.out.println(e);
	        }
		 
		 return lista;
	 }
	 
	public String agregarVehiculo(String curp, String matricula, String marca, String modelo, String caseta) {
	 	
		ConexionDB con=new ConexionDB();
		
		Bson filter =Filters.and(Filters.eq("curp",curp));
		
		System.out.println(curp+" "+matricula+" "+marca);
		
		Document newVehiculo = new Document()
				.append("matricula", matricula)
				.append("marca", marca)
				.append("modelo", modelo)
				.append("caseta", caseta);

		
		
	 try {
		 MongoCollection<Document> collection = con.crearConexion().getCollection("Arrendatarios");
		 collection.updateOne(filter,Updates.addToSet("vehiculo", newVehiculo));
		 return "Exito";
	} catch (Exception e) {
		return "Error"+e;
		// TODO: handle exception
	}
	 
 }

	public ArrayList<String[]> propiedadesDisponibles() {
		
		PropietarioDAO daop=new PropietarioDAO();
		 List<PropietarioM> propietarios=daop.ListaPropiedades();
		 ArrayList<Propiedad> propiedad=new ArrayList();
		 ArrayList<String[]> retorno=new ArrayList();
		 String[] a;
		 
	        try {           
	        	
	        	for (PropietarioM propiet : propietarios) {
	        		
	        		Propiedad[] propied=propiet.getPropiedad();
					for(Propiedad p:propied){
						if(p.getEstatus().equals("Disponible")) {
							a=new String[2];
							a[0]=p.getIdentificador();
							a[1]=propiet.getNombre();
							retorno.add(a);
						}
						
					}
				}
	        	
	        	
	        }catch(Exception e) {
	        	System.out.println(e);
	        }
	        
	        return retorno;
	}

	public void eliminaArrendatario(String curp) {
		ConexionDB con=new ConexionDB();


		try {           
			System.out.println("el curp en dao es:"+curp);
        	MongoCollection<Document> collection = con.crearConexion().getCollection("Arrendatarios");
        	Bson filter =Filters.and(Filters.eq("curp",curp));
        	
        	collection.findOneAndDelete(filter);

		} catch (Exception ex) {
            //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        	System.out.print("Fallo la conexion");
        }
	    
	  }
	
	public void eliminaV(String curp,String matricula) {
		ConexionDB con=new ConexionDB();


		try {           
			System.out.println("el curp en dao es:"+curp);
        	MongoCollection<Document> collection = con.crearConexion().getCollection("Arrendatarios");
        	Bson filter =Filters.and(Filters.eq("curp",curp));
        	collection.updateOne(filter,Updates.pull("vehiculo", new Document("matricula",matricula)));

		} catch (Exception ex) {
            //Logger.getLogger(PropietarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        	System.out.print("Fallo la conexion");
        }
	    
	  }
	
	/*
	public void updateP(String email, String newName) {
	    // Select the "people" collection
	    MongoCollection<Document> collection = db.getCollection("people");

	    // Create the document to specify find criteria
	    Document findDocument = new Document("email", email);

	    // Create the document to specify the update
	    Document updateDocument = new Document("$set",
	        new Document("name", newName));

	    // Find one person and delete
	    collection.findOneAndUpdate(findDocument, updateDocument);
	  }*/
	
}

package com.develop.controller;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PropietarioDAO;
import com.develop.config.ConexionDB;
import com.develop.model.Propiedad;
import com.develop.model.PropietarioM;
import com.develop.model.Vehiculo;
import com.develop.model.VehiculoM;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 * Servlet implementation class ConexionC
 */
@WebServlet("/PropietarioC")
public class PropietarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PropietarioC() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());

		PropietarioDAO dao = new PropietarioDAO();

		//////////////////////////////////// AGREGAR /////////////////////////////////
		if (request.getParameter("agregarPropietario") != null) {

			PropietarioM propietariom = new PropietarioM();
			Propiedad propiedadm = new Propiedad();
			Vehiculo vehiculo = new Vehiculo();
			
			propietariom.setNombre(request.getParameter("nombre"));
			propietariom.setApellidos(request.getParameter("apellido"));
			propietariom.setCurp(request.getParameter("curp"));
			propietariom.setEdad(request.getParameter("edad"));
			
			propiedadm.setDireccion(request.getParameter("direccion"));
			propiedadm.setEstatus(request.getParameter("estatus"));
			propiedadm.setCaseta(request.getParameter("caseta"));
			propiedadm.setIdentificador(request.getParameter("identificador"));
			String tiene = request.getParameter("check");

			propietariom.setCurp(request.getParameter("curp"));
			vehiculo.setMatricula(request.getParameter("matricula"));
			vehiculo.setMarca(request.getParameter("marca"));
			vehiculo.setModelo(request.getParameter("modelo"));
			vehiculo.setCaseta(request.getParameter("caseta"));
			System.out.println("El propietario ha agregado un vehiculo");

			String respuesta = dao.insertaPropietario(propietariom, propiedadm, vehiculo, tiene);
			if (respuesta.equals("Exito")) {
				RequestDispatcher rq = request.getRequestDispatcher(
						"http://localhost:8080/administracionSeccion/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq = request.getRequestDispatcher("/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq.forward(request, response);
			} else {
				RequestDispatcher rq = request.getRequestDispatcher(
						"http://localhost:8080/administracionSeccion/vistas/Error/FueraDeServicio.jsp");
				rq = request.getRequestDispatcher("/vistas/Error/FueraDeServicio.jsp");
				rq.forward(request, response);
			}
		}

		///////////////// LogIn ADMINISTRADORES//////////////

		if (request.getParameter("login") != null) {
			System.out.println("entre a propC");
			String usuario;
			String password;

			try {
				usuario = request.getParameter("nombre");
				password = request.getParameter("contrasena");

				String respuesta = "";// dao.validaUsuario(usuario, password);

				System.out.println(respuesta);

				if (respuesta.equals("OK")) {
					System.out.println("redireccionando");
					RequestDispatcher rq = request.getRequestDispatcher(
							"http://localhost:8080/administracionSeccion/vistas/PantallaGeneral.jsp");
					rq = request.getRequestDispatcher("/vistas/PantallaGeneral.jsp");
					rq.forward(request, response);

				} else {
					RequestDispatcher rq = request
							.getRequestDispatcher("http://localhost:8080/administracionSeccion/LogIn.jsp");
					rq = request.getRequestDispatcher("/vistas/LogIn.jsp");
					rq.forward(request, response);
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		///////////////////////////////////// AGREGAR VEHICULO ////////////////////////////////

		if (request.getParameter("agregarV") != null) {
			String curp;
			String matricula;
			String marca;
			String modelo;
			String caseta;

			curp = request.getParameter("curp");
			matricula = request.getParameter("matricula");
			marca = request.getParameter("marca");
			modelo = request.getParameter("modelo");
			caseta = request.getParameter("caseta");

			String respuesta = dao.agregarVehiculo(curp, matricula, marca, modelo, caseta);
			if (respuesta.equals("Exito")) {
				RequestDispatcher rq = request.getRequestDispatcher(
						"http://localhost:8080/administracionSeccion/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq = request.getRequestDispatcher("/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq.forward(request, response);
			} else {
				RequestDispatcher rq = request.getRequestDispatcher(
						"http://localhost:8080/administracionSeccion/vistas/Error/FueraDeServicio.jsp");
				rq = request.getRequestDispatcher("/vistas/Error/FueraDeServicio.jsp");
				rq.forward(request, response);
			}
		}

///////////////////////////////////// AGREGAR PROPIEDAD ////////////////////////////////

		if (request.getParameter("agregarP") != null) {
			String direccion;
			String estatus;
			String caseta;
			String identificador;
			String curp;

			direccion = request.getParameter("direccion");
			estatus = request.getParameter("estatus");
			caseta = request.getParameter("caseta");
			identificador = request.getParameter("identificador");
			curp = request.getParameter("curp");

			String respuesta = dao.agregarPropiedad(curp, direccion, estatus, caseta, identificador);
			if (respuesta.equals("Exito")) {
				RequestDispatcher rq = request.getRequestDispatcher(
						"http://localhost:8080/administracionSeccion/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq = request.getRequestDispatcher("/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq.forward(request, response);
			} else {
				RequestDispatcher rq = request.getRequestDispatcher(
						"http://localhost:8080/administracionSeccion/vistas/Error/FueraDeServicio.jsp");
				rq = request.getRequestDispatcher("/vistas/Error/FueraDeServicio.jsp");
				rq.forward(request, response);
			}
		}

		///////////////////////////////////// ELIMINA PROPIETARIO /////////////////

		if (request.getParameter("borrarP") != null) {
			String curp = request.getParameter("curp");

			dao.eliminaPropietario(curp);
		}

		/////////////////////////// ELIMINA VEHICULO ///////////////////////////
		if (request.getParameter("borrarV") != null) {
			String curp = request.getParameter("curp");
			String matricula = request.getParameter("matricula");

			dao.eliminaV(curp, matricula);
		}

		/////////////////////////// ELIMINA PROPIEDAD ///////////////////////////
		if (request.getParameter("borrarPP") != null) {
			String curp = request.getParameter("curp");
			String identificador = request.getParameter("identificador");

			dao.eliminaPropiedad(curp, identificador);
		}

		/////////////////

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

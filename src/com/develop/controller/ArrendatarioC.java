package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ArrendatarioDAO;
import com.develop.DAO.PropietarioDAO;
import com.develop.model.Arrendatario;
import com.develop.model.PropietarioM;
import com.develop.model.Vehiculo;

/**
 * Servlet implementation class ArrendatarioC
 */
@WebServlet("/ArrendatarioC")
public class ArrendatarioC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ArrendatarioC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
		ArrendatarioDAO dao= new ArrendatarioDAO();
		
		/////////////////////// AGREGA ARRENDATARIO /////////////////////
		if(request.getParameter("agregarA")!=null) {
		
		Arrendatario arrendatariom = new Arrendatario();
		Vehiculo vehiculom = new Vehiculo();
		arrendatariom.setNombre(request.getParameter("nombre"));
		arrendatariom.setApellidos(request.getParameter("apellido"));
		arrendatariom.setCurp(request.getParameter("curp"));
		arrendatariom.setEdad(request.getParameter("edad"));
		String[] propie=request.getParameter("propiedad").split("<");
		arrendatariom.setPropiedadRentada(propie[0]);
		String tiene=request.getParameter("check");
		System.out.println("casilla marcada?:"+tiene);
		
		
		vehiculom.setMatricula(request.getParameter("matricula"));
		vehiculom.setModelo(request.getParameter("modelo"));
		vehiculom.setMarca(request.getParameter("marca"));
		vehiculom.setCaseta(request.getParameter("caseta"));
		
		
		String respuesta = dao.insertaArrendatario(arrendatariom,vehiculom, tiene);
		if(respuesta.endsWith("Exito")) {
			RequestDispatcher rq = request.getRequestDispatcher("http://localhost:8080/administracionSeccion/vistas/Arrendatarios/PantallaVistaArrendatarios.jsp");
			rq = request.getRequestDispatcher("/vistas//Arrendatarios/PantallaVistaArrendatarios.jsp");
			rq.forward(request, response);
		}else {
			RequestDispatcher rq = request.getRequestDispatcher("http://localhost:8080/administracionSeccion/vistas/Propietarios/PantallaVistaPropietarios.jsp");
			rq = request.getRequestDispatcher("/vistas/Propietarios/PantallaVistaPropietarios.jsp");
			rq.forward(request, response);
		}
		}
		
		///////////////////////////AGREGA VEHICULO /////////////////////////////
		
		if(request.getParameter("agregarV")!= null) {
			String curp;
			String matricula;
			String marca;
			String modelo;
			String caseta;
			System.out.println("entre a controller");
			
			curp = request.getParameter("curp");
			matricula = request.getParameter("matricula");
			marca = request.getParameter("marca");
			modelo = request.getParameter("modelo");
			caseta = request.getParameter("caseta");
			
			String respuesta = dao.agregarVehiculo(curp, matricula, marca, modelo, caseta);
			if(respuesta.endsWith("Exito")) {
				RequestDispatcher rq = request.getRequestDispatcher("http://localhost:8080/administracionSeccion/vistas/Arrendatarios/PantallaVistaArrendatarios.jsp");
				rq = request.getRequestDispatcher("/vistas//Arrendatarios/PantallaVistaArrendatarios.jsp");
				rq.forward(request, response);
			}else {
				RequestDispatcher rq = request.getRequestDispatcher("http://localhost:8080/administracionSeccion/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq = request.getRequestDispatcher("/vistas/Propietarios/PantallaVistaPropietarios.jsp");
				rq.forward(request, response);
			}
		}
		
		/////////////////////////// ELIMINA ARRENDATARIO /////////////////////
		
	
		if(request.getParameter("borrarA")!= null) {
			String curp = request.getParameter("curp");
			
			
			dao.eliminaArrendatario(curp);
		}
		
		
		/////////////////////////// ELIMINA VEHICULO ///////////////////////////
		if(request.getParameter("borrarV")!= null) {
			String curp = request.getParameter("curp");
			String matricula = request.getParameter("matricula");
			
			
			dao.eliminaV(curp,matricula);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package com.develop.model;

public class Vehiculo {
	
	private String matricula;
    private String marca;
    private String modelo;
    private String caseta;

    public String getMatricula() { return matricula; }
    public void setMatricula(String value) { this.matricula = value; }

    public String getModelo() { return modelo; }
    public void setModelo(String value) { this.modelo = value; }
    
    public String getMarca() { return marca; }
    public void setMarca(String value) { this.marca = value; }

    public String getCaseta() { return caseta; }
    public void setCaseta(String value) { this.caseta = value; }

}

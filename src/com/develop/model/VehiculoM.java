package com.develop.model;

public class VehiculoM {

	private String matricula;
	private String modelo;
	
	public VehiculoM() {
		super();
	}

	public VehiculoM(String matricula, String modelo) {
		super();
		this.matricula = matricula;
		this.modelo = modelo;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
}

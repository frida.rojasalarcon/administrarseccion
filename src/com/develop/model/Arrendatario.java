package com.develop.model;

public class Arrendatario {
	

	private IdM id;
    private String nombre;
    private String apellidos;
    private String curp;
    private String edad;
    private String propiedadRentada;
    private Vehiculo[] vehiculo;

    public IdM getID() { return id; }
    public void setID(IdM value) { this.id = value; }

    public String getNombre() { return nombre; }
    public void setNombre(String value) { this.nombre = value; }

    public String getApellidos() { return apellidos; }
    public void setApellidos(String value) { this.apellidos = value; }

    public String getCurp() { return curp; }
    public void setCurp(String value) { this.curp = value; }

    public String getEdad() { return edad; }
    public void setEdad(String value) { this.edad = value; }

    public String getPropiedadRentada() { return propiedadRentada; }
    public void setPropiedadRentada(String value) { this.propiedadRentada = value; }

    public Vehiculo[] getVehiculo() { return vehiculo; }
    public void setVehiculo(Vehiculo[] value) { this.vehiculo = value; }
	

}

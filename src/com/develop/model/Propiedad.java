package com.develop.model;

public class Propiedad {
	
	private String direccion;
    private String estatus;
    private String caseta;
    private String identificador;

    public String getDireccion() { return direccion; }
    public void setDireccion(String value) { this.direccion = value; }

    public String getEstatus() { return estatus; }
    public void setEstatus(String value) { this.estatus = value; }

    public String getCaseta() { return caseta; }
    public void setCaseta(String value) { this.caseta = value; }
    
    public String getIdentificador() { return identificador; }
    public void setIdentificador(String value) { this.identificador = value; }

}

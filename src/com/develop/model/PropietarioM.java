package com.develop.model;

public class PropietarioM {
	
	
	
	private IdM id;
    private String nombre;
    private String apellidos;
    private String curp;
    private String edad;
    private Propiedad[] propiedad;
    private Vehiculo[] vehiculo;

    public IdM getID() { return id; }
    public void setID(IdM value) { this.id = value; }

    public String getNombre() { return nombre; }
    public void setNombre(String value) { this.nombre = value; }

    public String getApellidos() { return apellidos; }
    public void setApellidos(String value) { this.apellidos = value; }

    public String getCurp() { return curp; }
    public void setCurp(String value) { this.curp = value; }

    public String getEdad() {return edad;}
	public void setEdad(String edad) {this.edad = edad;}
	
	public Propiedad[] getPropiedad() { return propiedad; }
    public void setPropiedad(Propiedad[] value) { this.propiedad = value; }

    public Vehiculo[] getVehiculo() { return vehiculo; }
    public void setVehiculo(Vehiculo[] value) { this.vehiculo = value; }
	
}
